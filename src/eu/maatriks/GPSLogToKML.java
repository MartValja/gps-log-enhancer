package eu.maatriks;

import eu.maatriks.data.Placemark;
import eu.maatriks.io.SourceProcessor;
import eu.maatriks.io.XMLGenerator;

public class GPSLogToKML {

    public static void main(String[] args) {
        final long startTime = System.currentTimeMillis();
        XMLGenerator.build(Placemark.datapointsToPlacemarks(SourceProcessor.importSourceFiles()));
        final long endTime = System.currentTimeMillis();
        System.out.println("Completed in " + Long.valueOf(endTime - startTime).toString() + "ms.");
    }
}