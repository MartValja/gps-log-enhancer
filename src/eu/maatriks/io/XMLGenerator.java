package eu.maatriks.io;

import eu.maatriks.data.Datapoint;
import eu.maatriks.data.Placemark;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class XMLGenerator {

    private static final String NAME = "Kiirused";
    private static final String OUTPUT_FILENAME = "kaamera";

    public static void build(List<Placemark> placemarks) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.newDocument();

            // KML element
            Element kmlHeader = doc.createElement("kml");
            doc.appendChild(kmlHeader);
            Attr attr = doc.createAttribute("xmlns");
            attr.setValue("http://www.opengis.net/kml/2.2");
            kmlHeader.setAttributeNode(attr);

            // Document element (child of KML)
            Element document = doc.createElement("Document");
            kmlHeader.appendChild(document);
            attr = doc.createAttribute("id");
            attr.setValue("mart");
            document.setAttributeNode(attr);

            // Name element
            Element name = doc.createElement("name");
            document.appendChild(name);
            name.appendChild(
                    doc.createTextNode(NAME));

            // Description element
            Element desc = doc.createElement("description");
            document.appendChild(desc);
            desc.appendChild(
                    doc.createTextNode("Kiirusel üle " + SourceProcessor.SPEED_LIMIT + " km/h: "
                    +  Math.round(Placemark.getTotalDistanceCoveredInMeters() / 1000.0) + "km."));

            // Placemark elements
            int i = 1;
            for (Placemark placemark : placemarks) {
                Element placemarkElement = doc.createElement("Placemark");
                document.appendChild(placemarkElement);

                Element placemarkName = doc.createElement("name");
                placemarkElement.appendChild(placemarkName);
                placemarkName.appendChild(
                        doc.createTextNode(placemark.getName()));

                Element style = doc.createElement("Style");
                placemarkElement.appendChild(style);

                Element linestyle = doc.createElement("LineStyle");
                style.appendChild(linestyle);

                Element color = doc.createElement("color");
                linestyle.appendChild(color);
                color.appendChild(
                        doc.createTextNode(placemark.getHexLineColor()));

                Element width = doc.createElement("width");
                linestyle.appendChild(width);
                width.appendChild(
                        doc.createTextNode(Integer.toString(placemark.getLineWidth())));

                Element linestring = doc.createElement("LineString");
                placemarkElement.appendChild(linestring);
                attr = doc.createAttribute("id");
                attr.setValue(Integer.toString(i));
                linestring.setAttributeNode(attr);

                Element coordinates = doc.createElement("coordinates");
                linestring.appendChild(coordinates);
                coordinates.appendChild(
                        doc.createTextNode(datapointsToCoordinates(placemark.getDatapoints())));
                i++;
            }

            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            DOMSource source = new DOMSource(doc);
            Path outputFile = Paths.get(System.getProperty("user.dir") + "/" + OUTPUT_FILENAME + ".kml");
            StreamResult result =
                    new StreamResult(outputFile.toFile());
            transformer.transform(source, result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String datapointsToCoordinates(List<Datapoint> datapoints) {
        String result = "";
        for (Datapoint dp : datapoints)
            result += dp.getLongitude() + "," + dp.getLatitude() + " ";
        return result;
    }
}
