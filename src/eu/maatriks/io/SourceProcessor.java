package eu.maatriks.io;

import eu.maatriks.data.Datapoint;

import java.io.*;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.*;

public class SourceProcessor {

    private static final double NAUTICAL_TO_KMH = 1.852;
    public static final double SPEED_LIMIT = 94;
    private static final Path SOURCE_DIRECTORY = Paths.get(System.getProperty("user.dir") + "/source_log_files");
    private static final String LOGFILE_EXTENSION = "txt";

    /**
     * Imports all valid rows from GPS log files and returns them as Datapoint objects.
     * @return  List of valid Datapoint objects.
     */
    public static List<Datapoint> importSourceFiles() {
        List<Datapoint> allDatapoints = new ArrayList<>();
        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(SOURCE_DIRECTORY, "*." + LOGFILE_EXTENSION)) {
            for (Path sourceFilePath : directoryStream) {
                allDatapoints.addAll(sourceFileReader(sourceFilePath));
            }
        } catch (IOException ex) {
            System.out.println("Woopsie! IO Exception.");
        }
        return allDatapoints;
    }

    /**
     * Reads all valid rows from a single GPS log file and returns them as Datapoint objects.
     * @param sourceFilePath    Path of GPS log file.
     * @return                  List of Datapoint objects from given GPS log file.
     * @throws IOException
     */
    private static List<Datapoint> sourceFileReader(Path sourceFilePath) throws IOException {
        List<Datapoint> datapointsFromSingleFile = new ArrayList<>();
        BufferedReader br = Files.newBufferedReader(sourceFilePath);
        String line;
        Datapoint dp;
        Boolean newPlacemark = true;
        while ((line = br.readLine()) != null) {
            if ((dp = processLine(line)) == null) {
                newPlacemark = true;
            } else {
                dp.setNewPlacemarker(newPlacemark);
                datapointsFromSingleFile.add(dp);
                newPlacemark = false;
            }
        }
        br.close();
        return datapointsFromSingleFile;
    }

    /**
     * Checks if line is valid and in that case parses it into a Datapoint object.
     * @param line  A single line from GPS log as a String.
     * @return      Datapoint object, if valid line and over specificed speed. Otherwise null is returned.
     */
    private static Datapoint processLine(String line) {
        /* A,020315,171833.000,+5859.5891,N,+02449.5664,E,26.72,+00.00,-07.35,-78.40; */
        String[] split = line.split(",");
        if(split[0].toUpperCase().equals("A") && Double.valueOf(split[7]) * NAUTICAL_TO_KMH > SPEED_LIMIT) {
            int year = 2000 + Integer.valueOf(split[1].substring(4,6));
            int month = Integer.valueOf(split[1].substring(2,4));
            int day = Integer.valueOf(split[1].substring(0,2));
            int hour = Integer.valueOf(split[2].substring(0, 2));
            int minute = Integer.valueOf(split[2].substring(2, 4));
            int second = Integer.valueOf(split[2].substring(4, 6));
            int millisecond = Integer.valueOf(split[2].substring(7, 10));
            LocalDateTime timestamp = LocalDateTime.of(year, month, day, hour, minute, second, millisecond);
            double latitude = Double.valueOf(split[3].substring(0,3)) + Double.valueOf(split[3].substring(3)) / 60;
            double longitude = Double.valueOf(split[5].substring(0,4)) + Double.valueOf(split[5].substring(4)) / 60;
            return new Datapoint(latitude, longitude, timestamp);
        } else {
            return null;
        }
    }
}