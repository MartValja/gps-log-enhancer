package eu.maatriks.data;

import java.util.ArrayList;
import java.util.List;

public class Placemark {

    private static final int MAX_TIME_BETWEEN_DATAPOINTS = 5;

    private String name;
    private final String hexLineColor = "FF0000FF";
    private final int lineWidth = 3;
    private List<Datapoint> datapoints;
    private static int placemarkID = 0;
    private static double totalDistanceCoveredInMeters = 0;

    public String getName() {
        return name;
    }

    public String getHexLineColor() {
        return hexLineColor;
    }

    public int getLineWidth() {
        return lineWidth;
    }

    public List<Datapoint> getDatapoints() {
        return datapoints;
    }

    public static double getTotalDistanceCoveredInMeters() {
        return totalDistanceCoveredInMeters;
    }

    public static void increaseTotalDistance(double distanceInMeters) {
        Placemark.totalDistanceCoveredInMeters += distanceInMeters;
    }

    public Placemark(List<Datapoint> datapoints) {
        this.name = datapoints.get(0).getTimestamp().toLocalDate().toString() + "__" + ++placemarkID;
        this.datapoints = datapoints;
    }

    /**
     * Converts List of Datapoints to a List of Placemarks. New placemark is created if Datapoint comes
     * with a newPlaceMarker flag as 'true' (which indicates previous row in a file was either corrupt
     * or under specified speed) or if next Datapoint is more than 5 minutes in the future.
     * @param datapoints    Datapoints that will be converted.
     * @return              List of Placemark objects ready to be put into kml file.
     */
    public static List<Placemark> datapointsToPlacemarks(List<Datapoint> datapoints) {
        List<Placemark> placemarks = new ArrayList<>();
        List<Datapoint> tempList = new ArrayList<>();
        double tempDistanceInMeters = 0;
        for (int i = 0; i < datapoints.size() - 1; i++) {
            tempList.add(datapoints.get(i));
            if (datapoints.get(i + 1).getTimestamp().isBefore(
                    datapoints.get(i).getTimestamp().plusMinutes(MAX_TIME_BETWEEN_DATAPOINTS))) {
                tempDistanceInMeters += Datapoint.distanceBetweenDatapointsInMeters(datapoints.get(i), datapoints.get(i + 1));
                // If next datapoint is marked as a beginning of new Placemark, add current tempList to a Placemark and clean the list.
                if (datapoints.get(i + 1).getNewPlacemarker()) {
                    placemarks.add(new Placemark(tempList));
                    tempList = new ArrayList<>();
                    Placemark.increaseTotalDistance(tempDistanceInMeters);
                    tempDistanceInMeters = 0;
                }
            } else {
                placemarks.add(new Placemark(tempList));
                tempList = new ArrayList<>();
                Placemark.increaseTotalDistance(tempDistanceInMeters);
                tempDistanceInMeters = 0;
            }
        }
        // This only applies to the last element in the List.
        if (datapoints.size() > 1) {
            if (datapoints.get(datapoints.size() - 1).getTimestamp().isBefore(
                    datapoints.get(datapoints.size() - 2).getTimestamp().plusMinutes(MAX_TIME_BETWEEN_DATAPOINTS))) {
                tempList.add(datapoints.get(datapoints.size() - 1));
            }
        }
        if (tempList.size() > 0) {
            placemarks.add(new Placemark(tempList));
        }
        Placemark.increaseTotalDistance(tempDistanceInMeters);

        return placemarks;
    }
}
