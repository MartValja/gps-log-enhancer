package eu.maatriks.data;

import java.time.LocalDateTime;

public class Datapoint {

    private double latitude;
    private double longitude;
    private LocalDateTime timestamp;
    private boolean newPlacemarker;

    public Datapoint(double latitude, double longitude, LocalDateTime timestamp) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.timestamp = timestamp;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setNewPlacemarker(Boolean newPlacemarker) {
        this.newPlacemarker = newPlacemarker;
    }

    public boolean getNewPlacemarker() {
        return newPlacemarker;
    }

    public static double distanceBetweenDatapointsInMeters(Datapoint dp1, Datapoint dp2) {
        return VincentyDistance.getDistance(dp1.getLatitude(), dp1.getLongitude(), dp2.getLatitude(), dp2.getLongitude());
    }
}
